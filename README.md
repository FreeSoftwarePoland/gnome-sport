# Gnome Sport

Gnome Sport is a native Linux phone application to track your running and riding with GPS. 

# TODO
- [ ] record track ui
- [ ] record track backend
- [ ] phone deployment tool

## License
#### [GNU GPLv3 / Creative Commons BY-SA](./LICENSE)

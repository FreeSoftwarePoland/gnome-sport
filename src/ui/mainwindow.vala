/* mainwindow.vala
 *
 * Copyright 2022 Daniel Maksymow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GnomeSport.MainWindow : Adw.ApplicationWindow {
  private Gtk.Box container;
  private Adw.HeaderBar header;
  private Adw.ViewSwitcherTitle switcher_title;
  private Adw.ViewStack stack;
  private Gtk.Button button2;
  private RecordView record_view;

  public MainWindow(Adw.Application app) {
    Object(application: app);
    set_title("gnome-sport");
    set_default_size(400, 720); /* pinephone resolution */
    container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
    header = new Adw.HeaderBar();
    switcher_title = new Adw.ViewSwitcherTitle();
    initialize_stack();
    switcher_title.set_stack(stack);

    // var profile_page = stack.add_titled(button2, "profile", "Profile");
    header.set_title_widget(switcher_title);
    header.set_show_end_title_buttons(false);
    container.append(header);
    container.append(switcher_title);
    container.append(stack);
    set_content(container);
  }

  private void initialize_stack() {
    stack = new Adw.ViewStack();

    button2 = new Gtk.Button.with_label("test1");
    stack.add_titled(button2, "home", "Home");

    record_view = new RecordView();
    stack.add_titled(record_view, "record", "Record");
  }
}
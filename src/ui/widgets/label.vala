/* label.vala
 *
 * Copyright 2022 Daniel Maksymow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 public class GnomeSport.Label : Adw.Bin {
  private Gtk.Label _label;

  public Label(string label, string css_class) {
    _label = new Gtk.Label(label);
    unowned var style_context = _label.get_style_context();
    style_context.add_class(css_class);
    style_context.add_provider(CssProvider.instance(), Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    child = _label;
  }

  public void set_text(string text) {
    // stdout.printf("%d", &_label);
    stdout.flush();
    // FIXME: _label core dumping here
    // _label.set_label(text);
  }

  construct {
  }
 }
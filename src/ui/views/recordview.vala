/* recordview.vala
 *
 * Copyright 2022 Daniel Maksymow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

delegate void StartStopHandler();

public class GnomeSport.RecordView : Adw.Bin {
  private Label _distance;
  private Label _time;
  private Button _action;
  private Backend.TrackRecorder _record;
  private StartStopHandler _action_handler;

  public RecordView() {
    var content = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
    content.vexpand = true;
    content.halign = Gtk.Align.CENTER;
    init(content);
    child = content;
  }

  construct {
    _action_handler = start_recording;
    _record = new Backend.TrackRecorder.with_refresh_func(refresh_results);
  }

  private void refresh_results(Backend.RecordedData data) {
    string time = @"$(data.hours):$(data.minutes):$(data.seconds)";
    _time.set_text(time);
  }

  private void start_recording() {
    _record.record();
    _action.set_label("Stop");
    _action_handler = stop_recording;
  }

  private void stop_recording() {
    _record.stop();
    _action.set_label("Start");
    _action_handler = start_recording;
  }

  private void init(Gtk.Box content) {
    _distance = new Label("0", StyleClass.HUGE_LABEL);
    _distance.vexpand = true;
    _time = new Label("0:0:0", StyleClass.HUGE_LABEL);
    _time.vexpand = true;
    _action = new Button("Start", StyleClass.RECORD_BUTTON);
    _action.vexpand = false;
    _action.hexpand = false;

    content.append(new Label("Distance", StyleClass.LABEL));
    content.append(_distance);
    content.append(new Label("Time", StyleClass.LABEL));
    content.append(_time);
    content.append(_action);

    _action.clicked.connect(() => { _action_handler(); });
  }
}
[CCode (cheader_filename = "lib.h")]
namespace GnomeSport.Backend
{

[CCode (cname = "recorded_data_t")]
public struct RecordedData
{
  public uint8 hours;
  public uint8 minutes;
  public uint8 seconds;
  public uint8 kilometers;
  public uint8 meters;
}

[CCode (cname = "recorder_refresh_func", has_target = true)]
public delegate void RecordedDataRefreshFunc(RecordedData data);

[CCode (cname = "tracker_recorder_t", cprefix = "tracker_recorder_", free_function = "tracker_recorder_free")]
[Compact]
public class TrackRecorder
{
  [CCode (cname = "tracker_recorder_make")]
  public TrackRecorder.with_refresh_func(RecordedDataRefreshFunc func);

  public void record();

  public void stop();

  public bool get_last_record(RecordedData data);
}

}
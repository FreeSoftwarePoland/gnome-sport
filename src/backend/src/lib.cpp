/* lib.cpp
 *
 * Copyright 2022 Daniel Maksymow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lib.h"

#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>
#include <memory>
#include <functional>


namespace 
{
using namespace std::chrono_literals;


std::atomic_bool tracker_stop_flag = false;

class tracker_recorder
{
public:
  tracker_recorder(recorder_refresh_func);

  void record();

  recorded_data_t get_record() const;

  void stop();

private:
  static void thread_deleter(std::thread*);
  void recording_loop();

  std::chrono::seconds _seconds = 0s;
  recorder_refresh_func _refresh_func;
  using managed_thread_t = std::unique_ptr<std::thread, std::function<void(std::thread*)>>;
  managed_thread_t _recording_thread;
};

tracker_recorder::tracker_recorder(recorder_refresh_func func) 
  : _refresh_func(func)
{}

recorded_data_t tracker_recorder::get_record() const
{
  auto hours = std::chrono::duration_cast<std::chrono::hours>(_seconds);
  auto minutes = std::chrono::duration_cast<std::chrono::minutes>(_seconds - hours);
  auto seconds_left = _seconds - hours - minutes;
  recorded_data_t data = {
    .hours = static_cast<std::uint8_t>(hours.count()),
    .minutes = static_cast<std::uint8_t>(minutes.count()),
    .seconds =  static_cast<std::uint8_t>(seconds_left.count()),
  };

  return data;
}

void tracker_recorder::stop()
{
  tracker_stop_flag = true;
  _recording_thread.reset();
}

void tracker_recorder::thread_deleter(std::thread* ptr)
{
  ptr->join();
  delete ptr;
}

void tracker_recorder::recording_loop()
{
  while (!tracker_stop_flag) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    _seconds += 1s;
    recorded_data_t data = get_record();
    _refresh_func(&data);
  }
}

void tracker_recorder::record()
{
  tracker_stop_flag = false;
  _seconds = 0s;
  _recording_thread = managed_thread_t(new std::thread(&tracker_recorder::recording_loop, this), 
    &tracker_recorder::thread_deleter);
}

}


extern "C" {

tracker_recorder_t tracker_recorder_make(recorder_refresh_func f, void* userdata)
{
  std::cout << "tracker_recorder_make" << std::endl;
  return new tracker_recorder(f);
}

void tracker_recorder_record(tracker_recorder_t ptr)
{
  std::cout << "tracker_recorder_record: " << ptr << std::endl;
  static_cast<tracker_recorder *>(ptr)->record();
}

bool tracker_recorder_get_last_record(tracker_recorder_t ptr, recorded_data_t* data)
{
  std::cout << "tracker_recorder_get_last_record: " << ptr << std::endl;
  *data = static_cast<tracker_recorder *>(ptr)->get_record();
  return true;
}

void tracker_recorder_stop(tracker_recorder_t ptr)
{
  std::cout << "tracker_recorder_stop: " << ptr << std::endl;
  static_cast<tracker_recorder *>(ptr)->stop();
}

void tracker_recorder_free(tracker_recorder_t ptr)
{
  std::cout << "tracker_recorder_free" << std::endl;
  delete static_cast<tracker_recorder *>(ptr);
}

}
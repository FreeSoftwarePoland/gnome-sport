/* lib.h
 *
 * Copyright 2022 Daniel Maksymow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  uint8_t hours;
  uint8_t minutes;
  uint8_t seconds;
  uint8_t kilometers;
  uint8_t meters;
} recorded_data_t;

typedef void (*recorder_refresh_func)(recorded_data_t*);

typedef void* tracker_recorder_t;

tracker_recorder_t tracker_recorder_make(recorder_refresh_func, void*);

void tracker_recorder_record(tracker_recorder_t);

bool tracker_recorder_get_last_record(tracker_recorder_t, recorded_data_t*);

void tracker_recorder_stop(tracker_recorder_t);

void tracker_recorder_free(tracker_recorder_t);

#ifdef __cplusplus
}
#endif